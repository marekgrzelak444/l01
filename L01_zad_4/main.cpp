#include <iostream>
#include <cstdlib>

#include "fcn.hpp"

// definicja zmiennych globalnych

#define WYRAZ_START 0;
/*******************************************************************************
 * Funcjka głowna. Program sprawdza czy zadany tekst to palindrom
*******************************************************************************/

using namespace std;



int main (){
    string wyraz;                   // tekst wpisywany przez uzytkownika
    int wyraz_s,                    // znak poczatkowy
        wyraz_e;                    // znak koncowy
    bool palindrom;                 // war logiczna. Czy tekst to palidrom


    cout<<"\nTEST NA PALINDROM  \n"<<
        "  Wprowadz wyraz:  ";
    cin>>wyraz;

    if (wyraz.length()-1==0){         // Czy uzytkownik wprowadzil wyraz?
        cout<<"\n"<<"  Nie wprowadzono wyrazu. Koniec pracy programu";
        return 0;
    }

    wyraz_s=WYRAZ_START;
    wyraz_e=wyraz.length()-1;         // wliczenie dlugosci wyrazu

    if(palindrom=jestPal(wyraz, wyraz_s, wyraz_e))
        cout<<"\n"<<"  Wyraz jest palindromem. Koniec pracy programu."<<
            "\n";
    else
        cout<<"\n"<<"  Wyraz nie jest palidromem. Koniec pracy pragramu."<<
            "\n";


    // KONIEC PRACY PROGRAMU
    return 0;
}
