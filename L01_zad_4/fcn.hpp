#ifndef fcn_hpp
#define fcn_hpp

#include <iostream>
#include <string>
#include <string.h>

using namespace std;
/*******************************************************************************
 * Prototyp funcji testujacej czy palindrom.
*******************************************************************************/
bool jestPal(const string&,int,int);

#endif
