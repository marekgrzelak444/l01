#include <iostream>
#include "fcn.hpp"

/*******************************************************************************
 * Funkcja jestPal. Funckcja testuje slowo i sprawdza czy jest palindromem.
 * \param[in]       string            wyraz testowane slowo
 * \param[in]       int               wyraz_s wyraz poczatkowy
 * \param[in]       int               wyraz_e wyraz koncowy
 * \return          informacja logiczna, czy wyraz to palindrom;
*******************************************************************************/

bool jestPal(const string &wyraz,int wyraz_s,int wyraz_e){

    bool czyPal;
    if(wyraz_s<wyraz_e){
        if(tolower(wyraz[wyraz_s])==tolower(wyraz[wyraz_e])){
            czyPal=jestPal(wyraz,wyraz_s+1,wyraz_e-1);
            return czyPal;}
        else
            return false;
    }
    else
        return true;
}

