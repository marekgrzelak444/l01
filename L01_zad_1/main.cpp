#include <iostream>
#include <cstdlib>

#define WAR_MIN 0

using namespace std;
/*******************************************************************************
 * Funcjka wyswietl_menu wyswietlajaca menu programu. Funkcja typu void.
 * \brief funkcja wyswietlajaca menu programu.
*******************************************************************************/
void wyswietl_menu(){
    cout<<endl;
    cout<<"  1.  Wypelnij losowymi liczbami od 0 do x."<<endl;
    cout<<"  2.  Wyswietl zawartosc tablicy."<<endl;
    cout<<"  3.  Wyswietl wartosc maksymalna tablicy."<<endl;
    cout<<"  4.  Zakoncz."<<endl;
}

/*******************************************************************************
 * Funckcja Wypelnij_los. Funkcja typu void. Funcka wypelnia zadana tablice
 * pseudolosowa zawartoscia z zakresu zadanego przez uzytkownika.
 * \brief funkcja wypelniajaca tablice losowo
 *
 * \param[in]       const usigned int m     wysokosc tablicy
 * \param[in]       const usigned int n     szerokosc tablicy
 * \param[in/out]   int**         tablica   pseudolosowa tablica
*******************************************************************************/

void Wypelnij_los (const unsigned int m,const unsigned int n, int** tab){
    unsigned int i,j;                                       // iterator
    int x;                                                  // zakres 0-x
    cout<<"  Wybrano wypelnienie losowymi liczbami."<<endl;
    cout<<"  Wprowadz zakres liczb."<<endl<<endl<<"  ";
    cin>>x;
    cout<<endl;
    for(i=0;i<m;++i){                                       // losowe wypelnienie
        for(j=0;j<n;j++)
            tab[i][j]=(rand()%x)+WAR_MIN;
    }
}

/*******************************************************************************
 * Funcjka wyswietlajaca tablice. Funkcja typu void. funkcja wyswietla w
 * terminalu zawartosc tablicy.
 *
 * \param[in]       const usigned int m     wysokosc tablicy
 * \param[in]       const usigned int n     szerokosc tablicy
 * \param[in/out]   int**         tablica   pseudolosowa tablica
 *
*******************************************************************************/

void Wyswietl_tab (const unsigned int m, const unsigned int n, int** tab){
    unsigned int i,j;
    cout<<"  Wybralo wyswietlenie tablicy."<<endl;
    for (i=0;i<m;++i){                                      // wyswietlanie
        cout<<endl;                                         // tablicy
        for (j=0;j<n;++j){
            cout<<"  "<<tab[i][j];
        }
    }
    cout<<endl;
}

/*******************************************************************************
 * Funkcja wyszukujaca wartosc maksymalna tablicy. Funkcja typu void.
 * Funkcja wyswietla wartosc maksymalna tablicy i nie przechowje jej
 * w programie.
 *
 * \brief Funkcja wyszukujaca wartos maksymalna tablicy.
 *
 * \param[in]       const usigned int m     wysokosc tablicy
 * \param[in]       const usigned int n     szerokosc tablicy
 * \param[in/out]   int**         tablica   pseudolosowa tablica
 *
*******************************************************************************/

void War_max(const unsigned int m, const unsigned int n, int** tab){
    unsigned int i,j;
    int war_max=0;
    cout<<"  Wybrano wyswietlenie wartosci maksymalnej tablicy."<<endl;
    for(i=0;i<m;++i){
        for(j=0;j<n;j++)
            if(war_max<tab[i][j])
                war_max=tab[i][j];
    }
    cout<<"  Wartosc maksymalna tablicy:  "<<war_max<<endl;
}

/*******************************************************************************
 * Funcjka głowna. Program pozwala uzytkownikowi na okreslenie wielkosci tablicy dynamicznej
 * Program posłguje sie interfejsem getopt. Dostepne opcje:
*******************************************************************************/

int main(){
    unsigned int m,n;                   // wymiary tablicy, wysokosc, szerokosc
    unsigned int i;                     // iteratory petli
    int **tab;

    bool stop=false;                    // przelacznik while
    int wybor;                          // przelacznik switch

    cout<<endl<<"  Wprowadz wysokosc tablicy:  "<<endl<<endl<<"  ";
    cin>>m;
    cout<<endl<<"  Wprowadz szerokosc tablicy:  "<<endl<<endl<<"  ";
    cin>>n;

    //alokacja pamieci tablicy

    tab= new int *[m];
    for (i=0;i<m;++i)
        tab[i]=new int[n];

    wyswietl_menu();                     // 1 wyswietlenie menu

    while (!stop){                       // 2 czesc wykonawcza programu
        cout<<endl<<"  Wybierz opcje:  "<<endl<<endl<<"  ";
        cin>>wybor;
        cout<<endl;
        switch(wybor){
            case 1:
                Wypelnij_los(m,n,tab);break;
            case 2:
                Wyswietl_tab(m,n,tab);break;
            case 3:
                War_max(m,n,tab);break;
            case 4:
                cout<<"  Koniec pracy programu."<<endl<<endl;
                for (i=0;i<m;++i)
                    delete [] tab[i];
                delete [] tab;
                stop=true;
            break;

        }

    }
    return 0;
}
