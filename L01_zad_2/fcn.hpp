#ifndef fcn_hpp
#define fcn_hpp

#include <iostream>
#include <fstream>
#include <bitset>

/**********************************************************************
 * Prototyp funkcji odczytującej plik. Funkcja odczytuje plik i
 * wypisuje zawartosc w terminalu.
 * \brief Prototyp funkcji odczytujacej plik.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int ReadClas (const char* );

/**********************************************************************
 * Prototyp funkcji zapisujacej do pliku. Funkcja odczytuje dane
 * wprowadzone przez użytkownika i zapisuje je do pliku.
 * \brief Prototyp funkcji zapisujacej do pliku.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int WriteClas (const char* );

/**********************************************************************
 * Prototyp funkcji odczytującej plik binarny. Funkcja odczytuje plik i
 * wypisuje zawartosc w terminalu.
 * \brief Prototyp funkcji odczytujacej plik binarny.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int ReadBin (const char* );

/**********************************************************************
 * Prototyp funkcji zapisujacej do pliku binarnego. Funkcja odczytuje
 * dane wprowadzone przez użytkownika i zapisuje je do pliku.
 * \brief Prototyp funkcji zapisujacej do pliku binarnego.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int WriteBin (const char* );

/**********************************************************************
 * Prototyp funkcji zracajacej wielkosc napisu.
 * \brief Prototyp funkcji zwracajacej wielkość stringa.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int fileSize(const char *);

#endif