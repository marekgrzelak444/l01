#include <iostream>
#include <getopt.h>
#include <cstdlib>
#include "fcn.hpp"

using namespace std;
/*******************************************************************************
 * Program wykonuje  w oparciu o interface getopt:
 * odczyt z pliku do terminala
 * zapis do pliku z termiala
 * odczyt pliku binarnego
 * zapis do pliku binarnego
*******************************************************************************/
int main (int argc, char **argv){
    // getopt
    int opt;
    extern char *optarg;
    while ((opt = getopt (argc, argv, "a:b:c:d:")) != -1){
        switch (opt){
            case 'a':
                ReadClas(optarg);   // czytaj plik
                break;
            case 'b':
                WriteClas(optarg);  // zapisz do pliku
                break;
            case 'c':
                ReadBin(optarg);    // czytaj plik binarny
                break;
            case 'd':
                WriteBin(optarg);   // zapisz do pliku binarnego
                break;
            case '?':
                cout<<endl<<
                      "  BLAD: . . . Wybrano niepoprawna opcje. "
                      "Koniec pracy programu.  "<< endl;
                break;
            default:
                abort ();
        }

    }
  return 0;

}