#include "fcn.hpp"

/**********************************************************************
 * Funkcja odczytująca plik. Funkcja odczytuje plik i
 * wypisuje zawartosc w terminalu.
 * \brief Funkcja odczytujaca plik.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int ReadClas (const char * optarg){
    int *figure;                        // tablica znakow
    std::fstream file;                  // wejscie
    unsigned int i,j;                   // iteratory
    int filesize;                       // wielkosc string

    filesize = fileSize(optarg);        // pobierz wilekosc
    file.open(optarg, std::fstream::in);

    figure = new int [filesize];

    if (file.is_open()){                // jesli naza pliku poprawna
        std::cout<<"  Odczytano:"<<std::endl<<std::endl;
        i=0;
        while(!file.eof()){             // dopoki koniec pliku
            file >> figure[i];          // zaladuj tablice
            i++;
        }
        --i;
            for(j=0;j<i;++j){           // wyswietlenie tablicy
                std::cout<<"  "<<figure[j];
            }
        delete [] figure;               // usun tablice
            std::cout<<std::endl<<std::endl<<
                       "  Koniec pracy programu."<<std::endl;
        file.close();
        return 0;
    }
    else{                               // jesli nazwa pliku niepoprawna
        std::cerr<<std::endl<<
              "  BLAD: . . . Niepoprawna sciezka pliku."<<
              std::endl;
        delete [] figure;
        file.close();
        return 1;
    }
}

/**********************************************************************
 * Funkcja zapisujaca do pliku. Funkcja odczytuje dane
 * wprowadzone przez użytkownika i zapisuje je do pliku.
 * \brief Funkcja zapisujaca do pliku.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int WriteClas (const char * optarg){
    int *tab;                           //tablica liczb
    unsigned int i;                     // iterator
    std::fstream file;                  // wyjscie
    unsigned int NOFigure;              // liczba liczb

    file.open (optarg, std::fstream::out);

    if (file.is_open()){                // jesli nazwa pliku poprawna
        std::cout<<std::endl<<"  Wprowadz ilosc liczb:"<<std::endl;
        std::cin>>NOFigure;             // wprowadz liczba liczb
        tab = new int [NOFigure];       // nowa tablica
        std::cout<<std::endl<<"  Wprowadz tablice liczb:";

        for (i=0;i!=NOFigure;++i){      // wprowadz elem nalezace do tablicy
            std::cin>>tab[i];
        }
        for (i=0;i!=NOFigure;++i)       // wprowadz liczby do pliku
            file << "  "<< tab[i];
        delete [] tab;
        file.close();
        return 0;
    }
    else{                               // jesli nazwa niepoprawna
        std::cerr<<std::endl<<
              "  BLAD: . . . Niepoprawna sciezka pliku."<<
              std::endl;
        delete [] tab;
        file.close();
        return 1;
    }
}

/**********************************************************************
 * Funkcja odczytująca plik binarny. Funkcja odczytuje plik i
 * wypisuje zawartosc w terminalu.
 * \brief Funkcja odczytujaca plik binarny.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int ReadBin (const char * optarg){
    int *ptr=new int;                   //wskaznik na liczbe
    std::ifstream file;                 // wejscie

    file.open(optarg, std::ios::binary);

    if(file.is_open()){                 // jesli poprawna nazwa pliku
        while(!file.eof()){             // wykonaj dopoki koniec pliku
            file.read(( char *) ptr, sizeof (*ptr));
            if(!file.eof())             // jesli pobrana wartosc to nie
                                        // koniec
                std::cout<<*ptr<<std::endl;
        }
    }
    file.close();
    delete [] ptr;
    return 0;

}

/**********************************************************************
 * Funkcja zapisujaca do pliku binarnego. Funkcja odczytuje
 * dane wprowadzone przez użytkownika i zapisuje je do pliku.
 * \brief Funckja zapisujaca do pliku binarnego.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int WriteBin (const char * optarg){
    std::ofstream file;                 // wyjscie
    unsigned int i, NOFigure;           // iterator, liczba liczb
    int *tab;                           // tablica znakow
    file.open(optarg,std::ios::binary);

    if (file.is_open()){                // jesli poprawnie otwarty
        std::cout<<std::endl<<"  Wprowadz ilosc liczb:"<<std::endl;
        std::cin>>NOFigure;             // wprowadz liczbe liczb
        tab =new int [NOFigure];
        std::cout<<std::endl<<"  Wprowadz liczby:"<<std::endl;
        for( i=0; i<NOFigure; ++i)      // dopóki nie liczba liczb
            std::cin>> tab[i];

        for( i=0; i<NOFigure; i++){     // wprowadz do pliku binarnie
            file.write(( const char * ) & tab[i], sizeof tab[i] );
        }
        delete [] tab;

    }
    file.close();
    return 0;
}

/**********************************************************************
 * Funkcja zracajaca wielkosc napisu wprowadzonego przez uzytkownika.
 * \brief Funkcja zwracajaca wielkość stringa.
 * \param[in] const char* filename nazwa pliku
**********************************************************************/

int fileSize(const char *add){
    std::ifstream Source;           // sprawdzany plik
    Source.open(add, std::fstream::binary); //otworz plik
    Source.seekg(0,std::fstream::end);      // gdzie koniec
    int size = Source.tellg();              // zwroc wielkosc pliku.
    Source.close();
    return size;
}
